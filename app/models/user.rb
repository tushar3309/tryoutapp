class User < ActiveRecord::Base
  has_one :guardian, dependent: :destroy
  belongs_to :profile
  has_attached_file :photo
end
